import { LoginTypes } from './actions';
import { makeReducerCreator } from '../reduxCreator';

export const initialState = {
  isAuthenticated: false,
  roles: '',
  currentUser: null,
};

const loginSuccess = (state, action) => {
  return {
    ...state,
    isAuthenticated: true,
    currentUser: action.data,
  };
};

const loginFail = () => {
  return {
    ...initialState,
  };
};

export const auth = makeReducerCreator(initialState, {
  [LoginTypes.LOGIN_AUTH_SUCCESS]: loginSuccess,
  [LoginTypes.LOGIN_AUTH_FAIL]: loginFail,
});
