import { makeConstantCreator, makeActionCreator } from '../reduxCreator';

export const LoginTypes = makeConstantCreator(
  'LOGIN',
  'LOGIN_AUTH_FAIL',
  'LOGIN_AUTH_SUCCESS',
  'GET_CURRENT_USER',
  'LOGOUT',
  'LOGOUT_SUCCESS',
);

export const loginAction = () => makeActionCreator(LoginTypes.LOGIN);
export const loginSuccessAction = data =>
  makeActionCreator(LoginTypes.LOGIN_AUTH_SUCCESS, { data });
export const loginFailAction = () => makeActionCreator(LoginTypes.LOGIN_AUTH_FAIL);

export const getCurrentUserAction = () => makeActionCreator(LoginTypes.GET_CURRENT_USER);

export const logoutAction = () => makeActionCreator(LoginTypes.LOGOUT);
export const logoutSuccessAction = () => makeActionCreator(LoginTypes.LOGOUT_SUCCESS);
