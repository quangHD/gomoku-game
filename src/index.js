import React from 'react';
import Raven from 'raven-js';
import { LocaleProvider } from 'antd';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import 'antd/dist/antd.css';
import store, { history } from './redux/store';
import AppWrapper from './style';
import Home from './containers';

const target = document.querySelector('#root');

render(
  <LocaleProvider>
    <AppWrapper>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Home />
        </ConnectedRouter>
      </Provider>
    </AppWrapper>
  </LocaleProvider>,
  target,
);

Raven.config(process.env.REACT_APP_SENTRY_IO_URL).install();
