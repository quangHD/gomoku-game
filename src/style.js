import styled from 'styled-components';

const AppWrapper = styled.div`
  .container {
    padding-right: 25px;
    padding-left: 25px;
    margin-right: auto;
    margin-left: auto;
  }
  @media (min-width: 768px) {
    .container {
      width: 750px;
    }
  }
  @media (min-width: 992px) {
    .container {
      width: 970px;
    }
  }
  @media (min-width: 1200px) {
    .container {
      width: 1170px;
    }
  }

  @media (min-width: 1250px) {
    .container {
      width: 1200px !important;
    }
  }
`;

export default AppWrapper;
