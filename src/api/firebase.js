// Import the Firebase modules that you need in your app.
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

// Initalize and export Firebase.
const config = {
  apiKey: 'AIzaSyAjU5kauWqyo7uTr7s1bsVynv6WiD77vs8',
  authDomain: 'gomoku-80fea.firebaseapp.com',
  databaseURL: 'https://gomoku-80fea.firebaseio.com',
  projectId: 'gomoku-80fea',
  storageBucket: '',
  messagingSenderId: '189606038102',
};

const provider = new firebase.auth.FacebookAuthProvider();

export const loginFBAPI = () => {
  return firebase.auth().signInWithPopup(provider);
};

export default firebase.initializeApp(config);
