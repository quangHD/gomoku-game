import styled from 'styled-components';

const LoginWrapper = styled.div`
  width: 100%;
  display: flex;
  height: 100%;
  align-items: center;
  text-align: center;
  & > div {
    width: 100%;
    padding-bottom: 40px;
  }
  .title {
    font-size: 18px;
    margin-bottom: 20px;
  }
`;

export default LoginWrapper;
