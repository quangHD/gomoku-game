import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
// import { Button } from 'antd';

import Square, { X, O } from './Square';
import { repeatsTimes } from './util';

import { updateGameProcessAction } from '../../redux/game/actions';

class Board extends React.Component {
  replay = () => {
    const { board, prevBoards } = this.props;
    let i = 0;
    const interval = setInterval(advance.bind(this), 500);
    function advance() {
      this.props.updateGameProcess({
        board: prevBoards[(i += 1)] || board,
      });
      if (i === _.size(prevBoards)) {
        clearInterval(interval);
      }
    }
  };

  warn = () => {
    alert('STOP THAT');
  };

  win = mark => {
    if (mark === X) {
      alert('X wins');
    }
    if (mark === O) {
      alert('O wins');
    }
  };

  process = (x, y) => {
    const { board } = this.props;
    const mark = board[x][y];
    const won = this.won(mark, x, y);
    if (won) {
      this.props.updateGameProcess({
        won: mark,
      });
    }
  };

  won = (mark, x, y) => {
    const { board, winCondition } = this.props;
    const range = _.range(-winCondition + 1, winCondition);
    return _.some([
      repeatsTimes(
        _.map(range, i => {
          return _.get(board, [x, y - i]);
        }),
        mark,
        winCondition,
      ), // N-S
      repeatsTimes(
        _.map(range, i => {
          return _.get(board, [x + i, y - i]);
        }),
        mark,
        winCondition,
      ), // NE-SW
      repeatsTimes(
        _.map(range, i => {
          return _.get(board, [x + i, y]);
        }),
        mark,
        winCondition,
      ), // E-W
      repeatsTimes(
        _.map(range, i => {
          return _.get(board, [x + i, y + i]);
        }),
        mark,
        winCondition,
      ), // SE-NW
    ]);
  };

  play = (x, y) => {
    const { board, current, prevBoards } = this.props;
    if (board[x][y]) {
      return false;
    }

    const prevCurrent = current;
    prevBoards.push(_.cloneDeep(board));
    board[x][y] = current;
    this.props.updateGameProcess({
      board,
      current: current === X ? O : X,
      prevBoards,
      prevCurrent,
    });
    return true;
  };

  click = (x, y) => {
    const valid = this.play(x, y);
    if (!valid) {
      this.warn();
      return;
    }
    this.process(x, y);
  };

  render() {
    const click = this.click;
    const { board, won, current } = this.props;
    const { size } = this.props;
    return (
      <div>
        <div className="board-container">
          <div className={`board ${current}`}>
            {_.map(_.range(0, size), y => {
              return (
                <div className="board__row">
                  {_.map(_.range(0, size), x => {
                    return (
                      <Square
                        mark={_.get(board, [x, y])}
                        click={function() {
                          if (won) {
                            return;
                          }
                          click(x, y);
                        }}
                      />
                    );
                  })}
                </div>
              );
            })}
          </div>
        </div>
        {/* <div className="info-container">
          <div className="controls">
            {current === X && <p className="label">PLAYER TURN: X</p>}
            {current === O && <p className="label">PLAYER TURN: O</p>}
            {won === X && <h1>X won</h1>}
            {won === O && <h1>O won</h1>}
            {this.props.won && (
              <Button onClick={this.reset} className="btn btn--info">
                Restart Game
              </Button>
            )}
            {this.props.won && (
              <Button onClick={this.replay} className="btn btn--success">
                Replay
              </Button>
            )}
          </div>
        </div> */}
      </div>
    );
  }
}

Board.propTypes = {
  size: PropTypes.number,
  board: PropTypes.array,
  current: PropTypes.number,
  prevBoards: PropTypes.array,
  won: PropTypes.number,
  winCondition: PropTypes.number,
  updateGameProcess: PropTypes.func,
};

export default connect(
  state => {
    return {
      ...state.game,
    };
  },
  dispatch => {
    return {
      updateGameProcess: data => {
        dispatch(updateGameProcessAction(data));
      },
    };
  },
)(Board);
